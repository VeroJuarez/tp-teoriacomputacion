package main;

import java.util.HashMap;
import java.util.List;

import controller.ControllerPrincipal;
import model.domain.Produccion;
import view.Main_Window;

public class Main {
	static {
	     System.setProperty("java.awt.headless", "false");
	}

	public static void main(String[] args) {
		Main_Window vista = new Main_Window();
		ControllerPrincipal controlador = new ControllerPrincipal(vista);
		controlador.inicializar_Ventana_Principal();
	}
	private static String charArrayPrint(char[] charArr) {
		String ret = "";
		for(char c : charArr)
			ret += c+" ,";
		return ret;
	}
	
	public static void printMap2(HashMap<String, Produccion> hashmap) {
		
		hashmap.entrySet().forEach(entry2->{
			if(entry2.getValue() != null )
				System.out.println(" CHAR "+entry2.getKey()+" PROD "+ entry2.getValue().getVariable().getStringVariable() +" ->"+
		cuerpoToString(entry2.getValue().getCuerpo()));
		});	
	}
	
	public static String cuerpoToString(List<String> list) {
		String ret = "";
		
		for(String cuerpo : list)
			ret += cuerpo;
		
		return ret;
	}

}
