package controller;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileWriter;
import javax.swing.JFileChooser;

import model.EAFNDtoAFD.EAFNDtoAFD;
import model.domain.AFD;
import model.domain.Gramatica;
import model.input.InputParserReader;
import model.input.InputReader;
import model.parser.ParserSolver;
import model.parser.ParserTable;
import model.solver.AFDSolver;
import view.Automata_Window;
import view.Gramatica_Window;
import view.Main_Window;

public class ControllerPrincipal implements ActionListener {
	private Main_Window ventanaPrincipal = new Main_Window();
	private Automata_Window ventanaAutomatas = new Automata_Window();
	private Gramatica_Window ventanaGramaticas = new Gramatica_Window();
	private File archivoSeleccionado;
	private boolean automataConvertido = false;
	private AFD afd = null;
	private Gramatica gramatica = null;
	private ParserTable tabla_Parsing = null;
	
	public ControllerPrincipal(Main_Window vista) {
		this.ventanaPrincipal = vista;
		//this.ventanaPrincipal.getBtnAutomata().addActionListener(this);
		//this.ventanaPrincipal.getBtnGramaticas().addActionListener(this);
		this.addListeners();
		this.inicializar_Ventana_Principal();
	}
	
	public void addListeners() {
		this.ventanaPrincipal.getBtnAutomata().addActionListener(this);
		this.ventanaPrincipal.getBtnGramatica().addActionListener(this);
		this.ventanaAutomatas.getBtnSeleccionarArchivo().addActionListener(this);
		this.ventanaAutomatas.getBtnVolver().addActionListener(this);
		this.ventanaAutomatas.getBtnConvertir().addActionListener(this);
		this.ventanaAutomatas.getBtnGraficar().addActionListener(this);
		this.ventanaAutomatas.getBtnProcesar().addActionListener(this);
		this.ventanaAutomatas.getBtnGuardar().addActionListener(this);
		this.ventanaGramaticas.getBtnSeleccionarArchivo().addActionListener(this);
		this.ventanaGramaticas.getBtnVolver().addActionListener(this);
		this.ventanaGramaticas.getBtnProcesar().addActionListener(this);
	}
	
	public void inicializar_Ventana_Principal() {
		this.ventanaPrincipal.show();
	}
	
	public void inicializar_Ventana_Automatas() {
		this.ventanaAutomatas.show();
	}
	
	public void inicializar_Ventana_Gramaticas() {
		this.ventanaGramaticas.show();
	}
	
	public void actionPerformed(ActionEvent e) {
//Ventana principal
		if (e.getSource() == this.ventanaPrincipal.getBtnAutomata()) {
			this.ventanaPrincipal.getFrame().dispose();
			this.inicializar_Ventana_Automatas();
			this.vaciarTxtStrings();
		} else if (e.getSource() == this.ventanaPrincipal.getBtnGramatica()) {
			this.ventanaPrincipal.getFrame().dispose();
			this.inicializar_Ventana_Gramaticas();
		}
//ventana de automatas
		else if (e.getSource() == this.ventanaAutomatas.getBtnSeleccionarArchivo()) {
			System.out.println("SELECT ARCHIVO");
			JFileChooser fileChooser = new JFileChooser();
			int returnValue = fileChooser.showOpenDialog(null);
			if (returnValue == JFileChooser.APPROVE_OPTION) {
				archivoSeleccionado = fileChooser.getSelectedFile();
				this.ventanaAutomatas.getLblNombreArchivo().setText(archivoSeleccionado.getName());
				this.ventanaAutomatas.getBtnGraficar().setVisible(true);
				this.ventanaAutomatas.getBtnConvertir().setVisible(true);
				automataConvertido = false;
			}
			this.mostrarOcultarProcesarString(false);
		} else if (e.getSource() == this.ventanaAutomatas.getBtnConvertir()) {
			EAFNDtoAFD converter = new EAFNDtoAFD();
			this.afd = converter.fromAFNDtoAFD(InputReader.crearEAFND(archivoSeleccionado));
			this.automataConvertido = true;
			this.mostrarOcultarProcesarString(true);
			this.vaciarTxtStrings();
			this.ventanaAutomatas.getBtnGuardar().setVisible(true);
		} else if (e.getSource() == this.ventanaAutomatas.getBtnProcesar()) {
			String resultado = AFDSolver.resolver(afd, this.ventanaAutomatas.getTxtString().getText());
			if (resultado.equals("ACEPTADO"))
				this.ventanaAutomatas.getLblAceptarRechazar().setForeground(Color.GREEN);
			else
				this.ventanaAutomatas.getLblAceptarRechazar().setForeground(Color.RED);
			this.ventanaAutomatas.getLblAceptarRechazar().setText(resultado);
		} else if (e.getSource() == this.ventanaAutomatas.getBtnVolver()) {
			archivoSeleccionado = null;
			automataConvertido = false;
			this.ventanaAutomatas.getLblNombreArchivo().setText("");
			this.ventanaAutomatas.getBtnGraficar().setVisible(false);
			this.ventanaAutomatas.getBtnConvertir().setVisible(false);
			this.ventanaAutomatas.getBtnGuardar().setVisible(false);
			this.mostrarOcultarProcesarString(true);
			this.ventanaAutomatas.getFrame().dispose();
			this.inicializar_Ventana_Principal();
			this.mostrarOcultarProcesarString(false);
		} else if (e.getSource() == this.ventanaAutomatas.getBtnGuardar()) {
	
			JFileChooser fileChooser = new JFileChooser();
			int returnValue = fileChooser.showSaveDialog(null);
			if (returnValue == JFileChooser.APPROVE_OPTION) {
				try {
					FileWriter fw = new FileWriter(fileChooser.getSelectedFile() + ".txt");
					fw.write(this.afd.toString());
					fw.close();
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		}
//ventana de las gramaticas
		else if (e.getSource() == this.ventanaGramaticas.getBtnProcesar()) {
			String resultado = ParserSolver.resolver(this.ventanaGramaticas.getTxtString().getText(), this.tabla_Parsing);
			if (resultado.equals("ACEPTADO"))
				this.ventanaGramaticas.getLblAceptarRechazar().setForeground(Color.GREEN);
			else
				this.ventanaGramaticas.getLblAceptarRechazar().setForeground(Color.RED);
			this.ventanaGramaticas.getLblAceptarRechazar().setText(resultado);
		} else if (e.getSource() == this.ventanaGramaticas.getBtnSeleccionarArchivo()) {
			JFileChooser fileChooser = new JFileChooser();
			int returnValue = fileChooser.showOpenDialog(null);
			if (returnValue == JFileChooser.APPROVE_OPTION) {
				archivoSeleccionado = fileChooser.getSelectedFile();
				this.ventanaGramaticas.getLblNombreArchivo().setText(archivoSeleccionado.getName());
				this.gramatica = InputParserReader.crearGramatica(archivoSeleccionado);
				this.tabla_Parsing = new ParserTable(this.gramatica);
			}
			this.mostrarOcultarGramaticaString(true);
		} else if (e.getSource() == this.ventanaGramaticas.getBtnVolver()) {
			archivoSeleccionado = null;
			this.ventanaGramaticas.getLblNombreArchivo().setText("");
			this.mostrarOcultarProcesarString(true);
			this.ventanaGramaticas.getFrame().dispose();
			this.inicializar_Ventana_Principal();
			this.mostrarOcultarGramaticaString(false);
		}
	}
	
	private void mostrarOcultarProcesarString(boolean bool) {
		this.ventanaAutomatas.getLblAceptarRechazar().setVisible(bool);
		this.ventanaAutomatas.getLblResultado().setVisible(bool);
		this.ventanaAutomatas.getBtnProcesar().setVisible(bool);
		this.ventanaAutomatas.getTxtString().setVisible(bool);
		this.ventanaAutomatas.getLblString().setVisible(bool);
	}
	
	private void vaciarTxtStrings() {
		this.ventanaAutomatas.getLblAceptarRechazar().setText("");
		this.ventanaAutomatas.getTxtString().setText("");
	}
	
	private void mostrarOcultarGramaticaString(boolean bool) {
		this.ventanaGramaticas.getLblAceptarRechazar().setVisible(bool);
		this.ventanaGramaticas.getLblResultado().setVisible(bool);
		this.ventanaGramaticas.getBtnProcesar().setVisible(bool);
		this.ventanaGramaticas.getTxtString().setVisible(bool);
		this.ventanaGramaticas.getLblString().setVisible(bool);
	}

}
